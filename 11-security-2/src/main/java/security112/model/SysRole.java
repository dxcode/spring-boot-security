package security112.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SysRole {

    private Long id;
    private String role;

}